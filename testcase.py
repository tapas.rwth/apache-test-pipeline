from selenium import webdriver
import time
import unittest
from selenium.webdriver.firefox.options import Options
#import HtmlTestRunner
#from setup.config import Config

class MyTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        options = Options()
        options.headless=True
        cls.browser = webdriver.Firefox(options=options)
        #cls.browser = webdriver.Edge(r'..\drivers\MicrosoftWebDriver.exe')
        cls.browser.maximize_window()
        #input_data = Config()
        cls.browser.get("http://localhost:80")
        time.sleep(1)


    def test_search_text(self):
        self.search_field = self.browser.find_element_by_xpath("/html/body/h1")
        #input_data= Config()
        h1_text = self.search_field.text
        print(h1_text)
        self.assertEquals("Welcome to my test webserver",h1_text)

    @classmethod
    def tearDownClass(cls):
        time.sleep(2)
        cls.browser.quit()


if __name__ == '__main__':
    #op_folder = (r"C:\Users\tapas_jana\PycharmProjects\sample")
    #runner= HtmlTestRunner.HTMLTestRunner(output=op_folder, report_title="Test Result", descriptions= 'Sample Test')
    #unittest.main(testRunner=runner)
    unittest.main(verbosity=2)
