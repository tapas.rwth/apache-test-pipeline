This is a sample CI/CD project for building an apache webserver and deploy with docker:

Tools used: Gitlab (code repository and version controll), Jenkins(CI/CD pipeline), Docker (for containerize the application), Selenium+Python for unittest 
stages:

* SCM checkout: code (index.html, Dockerfile, unittest file in selenium+python) maintained in Gitlab repository, pulled to Jenkins 
* Build: Cleaning old environment, Building new Docker image from Dockerfile, Run container 
* Test: Unitesting the application with Selenium and Python 
* Deploy: Deploy the docker image
 
 ![small](/diagram.png)
